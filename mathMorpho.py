##=======================================================================================
# Name:        Victor Hugo Sillerico Justo
# No. USP:     11904461
# Course Code: SCC5830 (Image Processing)
# Year:        2020
# Semester:    I
# SHORT Assigment 3: Mathematical Morphology for Color Image Processing
# Git repository URL:    https://gitlab.com/vicoSJ/dip-sa3-morphology
##=======================================================================================

# libraries required
import numpy as np
import imageio

from skimage import morphology
from skimage.morphology import (square, rectangle, diamond, disk, star)
from skimage.color import rgb2hsv

# INPUT data
filename = str(input()).rstrip()
input_img = imageio.imread(filename) 
k = int(input())  # size of the structuring element
method = int(input()) # method to apply
# =================================================================

# Function to make opening operation
def openingOp(img,k):
    A = img
    B = morphology.disk(k) # create structuring element
    # applying erotion, and dilation
    AeB = morphology.erosion(A, B)
    AeBdB = morphology.dilation(AeB, B)
    return AeBdB

# Function to get morphological gradient
def MorphologicalGrad(img,k):
    A = img
    B = morphology.disk(k) # create structuring element
    # applying dilation, erotion, and substraction
    AdB = morphology.dilation(A, B)
    AeB = morphology.erosion(A, B)
    AgradB = np.subtract(AdB,AeB)
    return AgradB

# Function to get the root mean squared error of 1-channel image
def rmse(f,g):
    size = f.shape
    return np.sqrt(np.sum(np.square(f-g))/(size[0]*size[1]*size[2]))

# Function to apply opening operation over an RGB image
def RGB_opening(img,k):
    # get R, G, B channels 
    r, g, b = img[:, :, 0], img[:, :, 1], img[:, :, 2] 
    # apply opening over each channel 
    nb = openingOp(b,k)
    ng = openingOp(g,k)
    nr = openingOp(r,k)
    # composed a ner RGB image
    new_rgb = np.dstack((nr,ng,nb))
    return new_rgb

# Function to apply composition of operations
def compositionOp(img,k):
    # transform the image to the HSV space
    hsv_img = rgb2hsv(img)
    # get the HUE values
    hue_img = hsv_img[:, :, 0]
    # normalizing the H-channel to the interval 0 - 255
    minH = np.amin(hue_img)
    maxH = np.amax(hue_img)
    hue_norm = ((hue_img - minH)/(maxH - minH))*255  # normalized H-channel
    
    # with the structuring element disk, perform the morphological gradient operation
    # Morphological Gradient:= It is the difference between dilation and erosion of an image.
    mGrad_H = MorphologicalGrad(hue_norm,k) 
    
    # normalizing the resulting morphological gradient to the interval 0 - 255
    min_mGrad_H = np.amin(mGrad_H)
    max_mGrad_H = np.amax(mGrad_H)
    # the normalized gradient will be the new R-channel
    mGrad_H_norm = ((mGrad_H - min_mGrad_H)/(max_mGrad_H - min_mGrad_H))*255
    
    # applying opening to the normalized H-channel, it will be the new G-channel
    mdisk = morphology.disk(k) 
    opened_H_norm = morphology.opening(hue_norm, mdisk)

    # applying closing to the normalized H-channel, it will be the new B-channel
    closed_H_norm = morphology.closing(hue_norm, mdisk)
    
    new_rgb_m2 = np.dstack((mGrad_H_norm,opened_H_norm,closed_H_norm))
    return new_rgb_m2
# =================================================================


# SELECTION OF IMAGE PROCESSING

# 1. RGB opening
if method == 1:
	output_img = RGB_opening(input_img,k)

# 2. Composition of operations in RGB channels: gradient, opening, closing
if method == 2:
	output_img = compositionOp(input_img,k)

# 3. Using the two above procedures
if method == 3:
	# performing the RGB opening, size of the structuring element = 2*k
	kp = 2*k
	mod_img = RGB_opening(input_img,kp)		
	# using the opened image as input to the composition method, size of the structuring element = k
	output_img =  compositionOp(mod_img,k)

# =================================================================

# present the OUTPUT as it is required
# Getting the Root Square Error (RSE) with 4 decimals
print("%.4f" % (rmse(input_img.astype(float), output_img.astype(float))))

